import CheckEvent from "./CheckEvent";
import First from "./First";
import Navigation from "./Navigation";
import SpecialEvent from "./SpecialEvent";

const Header = () => {
    return (
        <div id="header" >
            <a href="#" id="logo"><img src="../../images/logo.jpg" alt="" /></a>
            <First />
            <CheckEvent />
            <Navigation />
            <img src="../images/lion-family.jpg" alt="" />
            <SpecialEvent />
        </div>
    )
}
export default Header;