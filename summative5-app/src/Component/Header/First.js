import React from "react";
import axios from "axios";

export default class First extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/getFirst')
            .then(res => {
                this.setState({
                    first: res.data
                });
            })
    }

    render() {
        return (
            <ul>
                {this.state.first.slice(0,3).map(arrFirst =>
                    <li>
                        <h2><a href="#">{arrFirst.title}</a></h2>
                        <span>{arrFirst.content}</span>
                    </li>
                )
                }
            </ul>
        )
    }
}