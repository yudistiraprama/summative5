const Navigation = () => {
    return (
        <ul id="navigation">
            <li id="link1" className="selected"><a href="#">Home</a></li>
            <li id="link2"><a href="#">The Zoo</a></li>
            <li id="link3"><a href="#">Visitors Info</a></li>
            <li id="link4"><a href="#">Tickets</a></li>
            <li id="link5"><a href="#">Events</a></li>
            <li id="link6"><a href="#">Gallery</a></li>
            <li id="link7"><a href="#">About Us</a></li>
        </ul>
    )
}

export default Navigation;