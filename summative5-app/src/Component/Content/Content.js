import Featured from "./Featured";
import Section1 from "./Section1";
import Section2 from "./Section2";
import Section3 from "./Section3";

const Content = () => {
    return (
        <div id="content">
            <Featured />
            <Section1 />
            <Section2 />
            <Section3 />
        </div>
    )
}
export default Content;