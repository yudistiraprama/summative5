import React from "react";
import axios from "axios";

export default class Featured extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gallery: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/getGallery')
            .then(res => {
                this.setState({
                    gallery: res.data
                });
            })
    }

    render() {
        return (
            <div id="featured" >
                <h2>Meet Out Animals</h2>
                <ul>
                    {this.state.gallery.slice(0, 8).map((arrGallery, indeks) =>
                    (indeks === 0 ?
                        <li className="first">
                            <a href="#"><img src={"../images/" + arrGallery.image} alt="" /></a>
                            <a href="#">{arrGallery.name}</a>
                        </li>
                        :
                        <li>
                            <a href="#"><img src={"../images/" + arrGallery.image} alt="" /></a>
                            <a href="#">{arrGallery.name}</a>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}