import React from "react";
import axios from "axios";

export default class Section1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sec: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/getSection1')
            .then(res => {
                this.setState({
                    sec: res.data
                });
            })
    }
    render() {
        return (
            <div className="section1" >
                <h2>Events</h2>
                <ul id="article">
                    {this.state.sec.slice(0, 3).map((arrSec, indeks) =>
                    (indeks === 0 ?
                        <li className="first">
                            <a href="#"><span>{arrSec.date}</span></a>
                            <p>{arrSec.content}</p>
                        </li>
                        :
                        <li>
                            <a href="#"><span>{arrSec.date}</span></a>
                            <p>{arrSec.content}</p>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}