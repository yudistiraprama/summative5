import React from "react";
import axios from "axios";

export default class Section21 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sec: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/getSection22')
            .then(res => {
                this.setState({
                    sec: res.data
                });
            })
    }

    render() {
        return (
            <div id="section2">
                <ul>
                    {this.state.sec.map(arrSec =>
                        <li>
                            <a href="#"><img src={"../images/"+arrSec.image} alt="" /></a>
                            <h4><a href="#">{arrSec.title}</a> </h4>
                            <p>{arrSec.content}</p>
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}