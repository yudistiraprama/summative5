import React from "react";
import Section21 from "./Section2/Section21";
import Section22 from "./Section2/Secton22";
import axios from "axios";

export default class Section2 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sec: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/getSection2')
            .then(res => {
                this.setState({
                    sec: res.data
                });
            })
    }
    render() {
        return (
            <div className="section2">
                {this.state.sec.slice(0, 1).map(arrSec =>
                    <>
                        <h2>{arrSec.title}</h2>
                        <p>{arrSec.content1}</p>
                        <a href=""><img src={"../images/"+arrSec.image} alt="" /></a>
                        <ul>
                            <li><p>{arrSec.content2}</p></li>
                        </ul>
                    </>
                )}
                <Section21 />
                <Section22 />
            </div>
        )
    }
}