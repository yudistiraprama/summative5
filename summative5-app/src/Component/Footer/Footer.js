import LeftContent from "./LeftContent";
import Navigation from "./Navigation";
import RightContent from "./RightContent";

const Footer = () => {
    return (
        <div id="footer">
            <div>
                <a href="#" className="logo"><img src="../images/animal-kingdom.jpg" alt="" /></a>
                <LeftContent />
                <Navigation />
                <RightContent />
                <p>Copyright &copy; 2021 | All Rights Reserved</p>
            </div>
        </div>
    )
}

export default Footer;