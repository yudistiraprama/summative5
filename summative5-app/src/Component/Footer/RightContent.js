const RightContent = () => {
    return (
        <ul>
            <li><a href="#">Live : Have fun in your visit</a></li>
            <li><a href="#">Love : Donate for the animals</a></li>
            <li><a href="#">Learn : Get to know the animals</a></li>
        </ul>
    )
}

export default RightContent;