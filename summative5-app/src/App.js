import './App.css';
import './Asset/css/style.css';
import Header from './Component/Header/Header';
import Footer from './Component/Footer/Footer';
import Content from './Component/Content/Content';

function App() {
  return (
    <div id="page">
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
