package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.Section2;
import co.id.nexsoft.summative5api.repository.Section2Repository;

@Service
public class Section2Service {
	@Autowired
	private Section2Repository section2Repo;

	public Section2 addSection2(Section2 section2) {
		return section2Repo.save(section2);
	}

	public List<Section2> getAllSection2() {
		return section2Repo.findAll();
	}

	public Section2 getSection2ById(int id) {
		return section2Repo.findById(id);
	}

	public void deleteSection2(int id) {
		section2Repo.deleteById(id);
	}

	public Section2 editSection2(int id, Section2 section2) {
		section2.setId(id);
		return section2Repo.save(section2);
	}
}
