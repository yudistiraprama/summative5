package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.Gallery;
import co.id.nexsoft.summative5api.service.GalleryService;

@RestController
@CrossOrigin
public class GalleryController {

	@Autowired
	private GalleryService galleryService;

//	GALLERY
	@PostMapping("/addGallery")
	public void addGallery(@RequestBody Gallery gallery) {
		galleryService.addGallery(gallery);
	}

	@PostMapping("/addGalleryList")
	public Iterable<Gallery> addGalleryList(@RequestBody List<Gallery> gallery) {
		return galleryService.addGalleryList(gallery);
	}

	@GetMapping("/getGallery")
	public List<Gallery> getGallery() {
		return galleryService.getAllGallery();
	}

	@GetMapping("/getGallery/{id}")
	public Gallery getGalleryById(@PathVariable int id) {
		return galleryService.getGalleryById(id);
	}

	@DeleteMapping("/deleteGallery/{id}")
	public void deleteGallery(@PathVariable int id) {
		galleryService.deleteGallery(id);
	}

	@PutMapping("/editGallery/{id}")
	public Gallery editGallery(@PathVariable int id, @RequestBody Gallery gallery) {
		return galleryService.editGallery(id, gallery);
	}

}
