package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.Section22;
import co.id.nexsoft.summative5api.service.Section22Service;

@RestController
@CrossOrigin
public class Section22Controller {

	@Autowired
	private Section22Service Section22Service;

//	SECTION 2
	@PostMapping("/addSection22")
	public void addSection22(@RequestBody Section22 Section22) {
		Section22Service.addSection22(Section22);
	}

	@GetMapping("/getSection22")
	public List<Section22> getSection22() {
		return Section22Service.getAllSection22();
	}

	@GetMapping("/getSection22/{id}")
	public Section22 getSection22ById(@PathVariable int id) {
		return Section22Service.getSection22ById(id);
	}

	@DeleteMapping("/deleteSection22/{id}")
	public void deleteSection22(@PathVariable int id) {
		Section22Service.deleteSection22(id);
	}

	@PutMapping("/editSection22/{id}")
	public Section22 editSection22(@PathVariable int id, @RequestBody Section22 Section22) {
		return Section22Service.editSection22(id, Section22);
	}
}
