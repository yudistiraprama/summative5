package co.id.nexsoft.summative5api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Summative5apiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Summative5apiApplication.class, args);
	}

}
