package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.First;
import co.id.nexsoft.summative5api.service.FirstService;

@RestController
@CrossOrigin
public class FirstController {

	@Autowired
	private FirstService firstService;

//	FIRST
	@PostMapping("/addFirst")
	public void addFisrt(@RequestBody First first) {
		firstService.addFirst(first);
	}

	@PostMapping("/addFirstList")
	public Iterable<First> addFirstList(@RequestBody List<First> first) {
		return firstService.addFirstList(first);
	}

	@GetMapping("/getFirst")
	public List<First> getFirst() {
		return firstService.getAllFirst();
	}

	@GetMapping("/getFirst/{id}")
	public First getFirstById(@PathVariable int id) {
		return firstService.getFirstById(id);
	}

	@DeleteMapping("/deleteFirst/{id}")
	public void deleteFirst(@PathVariable int id) {
		firstService.deleteFirst(id);
	}

	@PutMapping("/editFirst/{id}")
	public First editFirst(@PathVariable int id, @RequestBody First first) {
		return firstService.editFirst(id, first);
	}

}
