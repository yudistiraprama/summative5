package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.First;

@Repository
public interface FirstRepository extends CrudRepository<First, Integer> {
	List<First> findAll();

	First findById(int id);

	void deleteById(int Id);
}
