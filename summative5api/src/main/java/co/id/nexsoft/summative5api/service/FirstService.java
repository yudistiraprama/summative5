package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.First;
import co.id.nexsoft.summative5api.repository.FirstRepository;

@Service
public class FirstService {

	@Autowired
	private FirstRepository firstRepo;

	public First addFirst(First first) {
		return firstRepo.save(first);
	}

	public Iterable<First> addFirstList(List<First> firstList) {
		return firstRepo.saveAll(firstList);
	}

	public List<First> getAllFirst() {
		return firstRepo.findAll();
	}

	public First getFirstById(int id) {
		return firstRepo.findById(id);
	}

	public void deleteFirst(int id) {
		firstRepo.deleteById(id);
	}

	public First editFirst(int id, First first) {
		first.setId(id);
		return firstRepo.save(first);
	}

}
