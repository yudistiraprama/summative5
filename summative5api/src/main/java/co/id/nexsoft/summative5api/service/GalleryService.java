package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.Gallery;
import co.id.nexsoft.summative5api.repository.GalleryRepository;

@Service
public class GalleryService {

	@Autowired
	private GalleryRepository galleryRepo;

	public Gallery addGallery(Gallery gallery) {
		return galleryRepo.save(gallery);
	}

	public Iterable<Gallery> addGalleryList(List<Gallery> gallery) {
		return galleryRepo.saveAll(gallery);
	}

	public List<Gallery> getAllGallery() {
		return galleryRepo.findAll();
	}

	public Gallery getGalleryById(int id) {
		return galleryRepo.findById(id);
	}

	public void deleteGallery(int id) {
		galleryRepo.deleteById(id);
	}

	public Gallery editGallery(int id, Gallery gallery) {
		gallery.setId(id);
		return galleryRepo.save(gallery);
	}
}
