package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.Section1;

@Repository
public interface Section1Repository extends CrudRepository<Section1, Integer> {
	List<Section1> findAll();

	Section1 findById(int id);

	void deleteById(int Id);
}
