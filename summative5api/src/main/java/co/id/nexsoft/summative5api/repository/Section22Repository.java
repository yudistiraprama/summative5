package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.Section22;

@Repository
public interface Section22Repository extends CrudRepository<Section22, Integer> {
	List<Section22> findAll();

	Section22 findById(int id);

	void deleteById(int Id);
}
