package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.Section2;
import co.id.nexsoft.summative5api.service.Section2Service;

@RestController
@CrossOrigin
public class Section2Controller {

	@Autowired
	private Section2Service section2Service;

//	SECTION 2
	@PostMapping("/addSection2")
	public void addSection2(@RequestBody Section2 section2) {
		section2Service.addSection2(section2);
	}

	@GetMapping("/getSection2")
	public List<Section2> getSection2() {
		return section2Service.getAllSection2();
	}

	@GetMapping("/getSection2/{id}")
	public Section2 getSection2ById(@PathVariable int id) {
		return section2Service.getSection2ById(id);
	}

	@DeleteMapping("/deleteSection2/{id}")
	public void deleteSection2(@PathVariable int id) {
		section2Service.deleteSection2(id);
	}

	@PutMapping("/editSection2/{id}")
	public Section2 editSection2(@PathVariable int id, @RequestBody Section2 section2) {
		return section2Service.editSection2(id, section2);
	}
}
