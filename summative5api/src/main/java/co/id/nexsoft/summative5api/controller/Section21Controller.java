package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.Section21;
import co.id.nexsoft.summative5api.service.Section21Service;

@RestController
@CrossOrigin
public class Section21Controller {

	@Autowired
	private Section21Service Section21Service;

//	SECTION 2
	@PostMapping("/addSection21")
	public void addSection21(@RequestBody Section21 Section21) {
		Section21Service.addSection21(Section21);
	}

	@GetMapping("/getSection21")
	public List<Section21> getSection21() {
		return Section21Service.getAllSection21();
	}

	@GetMapping("/getSection21/{id}")
	public Section21 getSection21ById(@PathVariable int id) {
		return Section21Service.getSection21ById(id);
	}

	@DeleteMapping("/deleteSection21/{id}")
	public void deleteSection21(@PathVariable int id) {
		Section21Service.deleteSection21(id);
	}

	@PutMapping("/editSection21/{id}")
	public Section21 editSection21(@PathVariable int id, @RequestBody Section21 Section21) {
		return Section21Service.editSection21(id, Section21);
	}
}
