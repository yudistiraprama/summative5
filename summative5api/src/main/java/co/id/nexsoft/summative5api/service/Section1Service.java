package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.Section1;
import co.id.nexsoft.summative5api.repository.Section1Repository;

@Service
public class Section1Service {

	@Autowired
	private Section1Repository section1Repo;

	public Section1 addSection1(Section1 section1) {
		return section1Repo.save(section1);
	}

	public Iterable<Section1> addSection1List(List<Section1> section1) {
		return section1Repo.saveAll(section1);
	}

	public List<Section1> getAllSection1() {
		return section1Repo.findAll();
	}

	public Section1 getSection1ById(int id) {
		return section1Repo.findById(id);
	}

	public void deleteSection1(int id) {
		section1Repo.deleteById(id);
	}

	public Section1 editSection1(int id, Section1 section1) {
		section1.setId(id);
		return section1Repo.save(section1);
	}
}
