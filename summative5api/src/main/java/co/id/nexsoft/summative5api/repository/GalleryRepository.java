package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.Gallery;

@Repository
public interface GalleryRepository extends CrudRepository<Gallery, Integer> {
	List<Gallery> findAll();

	Gallery findById(int id);

	void deleteById(int Id);
}
