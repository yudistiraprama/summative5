package co.id.nexsoft.summative5api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.nexsoft.summative5api.model.Section1;
import co.id.nexsoft.summative5api.service.Section1Service;

@RestController
@CrossOrigin
public class Section1Controller {

	@Autowired
	private Section1Service section1Service;

//	SECTION 1
	@PostMapping("/addSection1")
	public void addSection1(@RequestBody Section1 section1) {
		section1Service.addSection1(section1);
	}

	@PostMapping("/addSection1List")
	public Iterable<Section1> addSection1List(@RequestBody List<Section1> section1) {
		return section1Service.addSection1List(section1);
	}

	@GetMapping("/getSection1")
	public List<Section1> getSection1() {
		return section1Service.getAllSection1();
	}

	@GetMapping("/getSection1/{id}")
	public Section1 getSection1ById(@PathVariable int id) {
		return section1Service.getSection1ById(id);
	}

	@DeleteMapping("/deleteSection1/{id}")
	public void deleteSection1(@PathVariable int id) {
		section1Service.deleteSection1(id);
	}

	@PutMapping("/editSection1/{id}")
	public Section1 editSection1(@PathVariable int id, @RequestBody Section1 section1) {
		return section1Service.editSection1(id, section1);
	}
}
