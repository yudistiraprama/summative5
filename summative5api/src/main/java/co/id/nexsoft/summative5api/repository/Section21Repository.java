package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.Section21;

@Repository
public interface Section21Repository extends CrudRepository<Section21, Integer> {
	List<Section21> findAll();

	Section21 findById(int id);

	void deleteById(int Id);
}
