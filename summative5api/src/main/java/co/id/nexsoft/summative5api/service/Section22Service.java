package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.Section22;
import co.id.nexsoft.summative5api.repository.Section22Repository;

@Service
public class Section22Service {
	@Autowired
	private Section22Repository Section22Repo;

	public Section22 addSection22(Section22 Section22) {
		return Section22Repo.save(Section22);
	}

	public List<Section22> getAllSection22() {
		return Section22Repo.findAll();
	}

	public Section22 getSection22ById(int id) {
		return Section22Repo.findById(id);
	}

	public void deleteSection22(int id) {
		Section22Repo.deleteById(id);
	}

	public Section22 editSection22(int id, Section22 Section22) {
		Section22.setId(id);
		return Section22Repo.save(Section22);
	}
}
