package co.id.nexsoft.summative5api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ApiController {

	@GetMapping("/")
	public String index() {
		return "<h1>WELCOME</h1>";
	}
}
