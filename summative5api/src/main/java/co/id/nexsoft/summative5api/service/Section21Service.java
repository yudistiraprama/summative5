package co.id.nexsoft.summative5api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.id.nexsoft.summative5api.model.Section21;
import co.id.nexsoft.summative5api.repository.Section21Repository;

@Service
public class Section21Service {
	@Autowired
	private Section21Repository Section21Repo;

	public Section21 addSection21(Section21 Section21) {
		return Section21Repo.save(Section21);
	}

	public List<Section21> getAllSection21() {
		return Section21Repo.findAll();
	}

	public Section21 getSection21ById(int id) {
		return Section21Repo.findById(id);
	}

	public void deleteSection21(int id) {
		Section21Repo.deleteById(id);
	}

	public Section21 editSection21(int id, Section21 Section21) {
		Section21.setId(id);
		return Section21Repo.save(Section21);
	}
}
