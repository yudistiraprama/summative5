package co.id.nexsoft.summative5api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.id.nexsoft.summative5api.model.Section2;

@Repository
public interface Section2Repository extends CrudRepository<Section2, Integer> {
	List<Section2> findAll();

	Section2 findById(int id);

	void deleteById(int Id);
}
